Git - Introduction
==================

Version Control System : Tracking of a folder (called repository) shared by multiple parties
git uses a hidden .git folder inside the repository itself (init)
Inside all versions of the repository are stored efficiently via diffs.
Everyone has the whole history locally available.
There's nothing special about a repository hosted on a server to the one that's local to a user.

Terminology
-----------

repository : A folder with a .git folder init
.gitignore : A fild in this folder, which specifies files/folders to be excluded from version control
commit : The head of a history of the repository state.
HEAD : The commit currently 'checked-out', i.e. the files in the repository
master : Name for the commit, considered the official state of the repository
remote : An external other git repository, most of the time hosted on a server
branch : A commit ,i.e. a history of changes independent of master
head : Name of commits in the repository: HEAD, master, and every branch are heads.
tag : Another mechanism to name commits in the history, (i.e. to mark a commit as being version 1.0.2)
origin : Name of the repository you initially cloned from

Commands
-------

init : create an empty repository
clone : clone a remote repository to start working on it
add : add files to the tracked index of a repository
reset : checkout a different version in the history
fetch : download changes from a remote
pull : fetch and integrate changes into repository
push : broadcast local commits to a remote
merge : combine two branches into one
tag : list all tags
branch : list all branches
show : list of the latest commit in the branch of HEAD

Examples
--------

Get help:

    git help

    git help *command-name*

Download a Git repository

    git clone *url*

Get latest changes from origin

    git pull

Show your branches

    git branch

List remote branches
    
    git branch -r

List all tags

    git tag

Switch to another branch, commit

    git checkout *branchname/tag*

Reset state of repository to a previous state

    git reset --hard *tagname*
    git clean -fdx

Show commits

    git show
    git log

Tell git who you are

    git config --global user.name "Gabriel Heinrich"
    git config --global user.email g.heinrich@allblock.org

Create a new git repository

    git init

Show your changes

    git status

Add all files to the index

    git add .

Commit changes

    git commit -a

Create a new branch (while staying on current branch)

    git branch *branchname*
