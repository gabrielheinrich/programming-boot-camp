#include <stdio.h>

int fac (int x)
{
    int result = 1;
    while (x > 0) {
        result *= x;
        --x;
    }
    return result;
}

int main() {
    printf ("%i", fac (7));
    return 0;
}
