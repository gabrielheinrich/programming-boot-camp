        LOC Data_Segment
BOS     GREG #3000000000000000
SP      GREG 0
RET     GREG 0

        LOC #100
        GREG @
fac     SUB SP,SP,16
        STO $0,SP,8
        
        SET $0,1
        STT $0,SP

loop    LDT $0,SP,16
        CMP $0,$0,0
        BNP $0,loopend

        LDT $0,SP,0
        LDT $1,SP,16
        MUL $0,$0,$1
        STT $0,SP

        LDT $0,SP,16
        SUB $0,$0,1
        STT $0,SP,16

        JMP loop

loopend LDT RET,SP,0
        LDO $0,SP,8
        ADD SP,SP,16
        GO $0,$0,0

Main    SET SP,BOS
        SUB SP,SP,8

        SET $0,5
        STT $0,SP,0

        GO $0,fac

        ADD SP,SP,8

        TRAP 0,Halt,0
