        LOC Data_Segment
        GREG @
BOS     GREG #3000 0000 0000 0000
SP      GREG 0

        LOC #100
Main    SET SP,BOS

        SUB SP,SP,8

        SET $0,12
        STT $0,SP,4
        
        LDT $0,SP,4
        SET $1,14
        ADD $0,$0,$1
        STT $0,SP,0

        LDT $0,SP,4
        ADD $0,$0,1
        STT $0,SP,4

        TRAP 0,Halt,0
