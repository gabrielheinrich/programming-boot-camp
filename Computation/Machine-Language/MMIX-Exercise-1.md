MMIX - Exercise 1
=================

1. Install mmix and write, compile and run a Hello World Program: [Tutorial](http://mmix.cs.hm.edu/examples/hello.html)

2. Write a C function, which computes a fibonacci number and a main function, which calls this function with the argument 10.
   Try to compile and run it.

```C
        int fibonacci (int n) {
            /* your code goes here*/
        }

        void main() {
            fibonacci (10);
        }
```

3. Translate this program into mmix.

4. Try to write an alternative more efficient version of this main function.

5. Commit all the files you created to your git repository.
