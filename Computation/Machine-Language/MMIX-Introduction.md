MMIX-Introduction
=================

References
==========

Introduction by Donald Knuth:
http://mmix.cs.hm.edu/doc/fasc1.pdf

Detailed Specification:
http://mmix.cs.hm.edu/doc/mmix-doc.pdf

Quick Reference:
http://mmix.cs.hm.edu/doc/mmix-refcard-a4.pdf

Hello World:
http://mmix.cs.hm.edu/examples/hello.html

Overview
========

* four byte instructions
* Formats : 
    opcode | $X | $Y | $Z
    opcode | $X | YZ
    opcode | XYZ

* Often used Address = Base + Offset as Address = $Y + Z

* Machine Number Terminology
    Byte : 8bit
    Wyde : 16bit
    Tetra : 32bit
    Octa : 64bit

* 255 General Purpose Regsiters
* 255 Special Registers

Instructions
============

Load
----
LDB $X $Y $Z : Load Byte from address $Y + $Z into register $X
LDW $X $Y $Z : Load Wyde from address $Y + $Z into register $X
LDT $X $Y $Z : Load Tetra from address $Y + $Z into register $X
LDO $X $Y $Z : Load Octa from address $Y + $Z into register $X

Store
-----
STB $X $Y $Z : Store Byte in register $X at address $Y + $Z
STW $X $Y $Z : Store Wyde in register $X at address $Y + $Z
STT $X $Y $Z : Store Tetra in register $X at address $Y + $Z
STO $X $Y $Z : Store Octa in register $X at address $Y + $Z

Setting Registers
-----------------
GET $X Z : Move data from special register with number Z to register $X
PUT X $Z : Move data from register $Z into special register X
SET $X $Z : Move data from register $Z to register $X
SET $X YZ : Move immediate constant YZ to regsiter %X

Load address
------------
LDA $X $Y $Z : Store $Y + Z in $X

Arithmetic
----------
ADD $X $Y $Z : $X = $Y + $Z
SUB $X $Y $Z : $X = $Y - $Z
MUL $X $Y $Z : $X = $Y * $Z
DIV $X $Y $Z : $X = $Y / $Z
NEG $X $Z : $X = - $Z

Compare
-------
CMP $X $Y $Z :
    $X = 0 <=> $Y = $Z
    $X = -1 <=> $Y < $Z
    $X = 1 <=> $Y > $Z

Bitwise Operations
------------------

AND $X $Y $Z
OR $X $Y $Z
XOR $X $Y $Z
NOR $X $Y $Z
NAND $X $Y $Z
ANDN $X $Y $Z
NXOR $X $Y $Z
NOR $X $Y $Z
SL $X $Y $Z : bitshift left
SR $X $Y $Z : bitshift right

Jumps
-----

JMP XYZ : Continue execution at current position + XYZ, often specified as a label (assembler figures out offset)
GO $X $Y $Z : Continue execution at address $Y + $Z and store address of current position + 4 in $X

Branches
--------

BZ $X $Y $Z : if $X = 0 go to relative address $Y + $Z
BP : Branch if positive
BN : Branch if negative
BEV : Branch if even
BOD : Branch if odd
BNN : Branch if non negative
BNP : Branch if non positive
BNZ : Branch if non zero
