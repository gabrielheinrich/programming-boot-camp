Programming Boot Camp
=====================

This is a collection of reference material, which eventually shall comprise
everything one needs to study to consider oneself a proficient programmer.

Outline
=======

I Tools
-------

  We want to cover a basic toolset a programmer needs to get under one's belt
  to work fluently:

  * Text Editor / IDE : VIM
  * Terminal : Unix Command Line Tools
  * Compiler : GCC
  * Version Control System : GIT
  * Debugging : GDB, Lint
  * C Tools : Clang Formatter
  * Build System : Make, CMake

II Computation
--------------

  Before diving into higher level languages, we want to get a good grip on the
  basics of computation:

  * Machine Numbers and Operations
  * Computer Architecture
  * Machine Language : MMIX
  * Low Level Imperative Programming : C
  * Operating Systems : Unix

III Languages & Paradigms
-------------------------

  The next goal is to get fluent in reading and writing a vast set of languages.
  We hope to achieve this by studying a small set of highly influential languages
  and the paradigms they support:

  * Multi-Paradigm
    * Statically Typed : C++
    * Dynamically Typed : Python
  * Functional/Logical/Declarative
    * Statically Typed : Haskell
    * Dynamically Typed : Clojure
  * Web-Languages
    * HTML
    * CSS
    * Javascript
  
IV Applications
---------------

  The last open-ended part will be to study real life projects from different fields.
  This is the essential part of the program.

  * Compilers and Runtimes : Clang, GHC, Python, LUA, JVM
  * GUI Frameworks & Applications
  * Web Frameworks & Applications
  * Abstract Data Structures
  * Commandline Tools : grep, GIT
  * Kernel : Linux
  * Web Browser : Google Chrome
  * ...
